package com.zeuslab.vktasks;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yatarkan on 12.02.2016.
 */
public class DownloadImages extends AsyncTask<String,Void,Bitmap> {

    private final WeakReference<CircleImageView> imageViewReference;

    //CircleImageView bmImage;

    public DownloadImages(CircleImageView imageView) {
        //this.bmImage = bmImage;
        imageViewReference = new WeakReference<CircleImageView>(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return downloadBitmap(params[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }

        if (imageViewReference != null) {
            CircleImageView imageView = imageViewReference.get();
            if (imageView != null) {
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                } else {
                    Drawable placeholder = imageView.getContext().getResources().getDrawable(R.drawable.user_profile_default);
                    imageView.setImageDrawable(placeholder);
                }
            }
        }
    }

    private Bitmap downloadBitmap(String url) {
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            int statusCode = urlConnection.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                return null;
            }

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
                return bitmap;
            }
        } catch (Exception e) {
            //urlConnection.disconnect();
            Log.w("DownloadImages", "Error downloading image from " + url);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }


    public static Bitmap downloadImageBitmap(String url){

        HttpURLConnection connection    = null;
        InputStream is                  = null;

        try {
            URL get_url     = new URL(url);
            connection      = (HttpURLConnection) get_url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();
            is              = new BufferedInputStream(connection.getInputStream());
            final Bitmap bitmap = BitmapFactory.decodeStream(is);
            //return bitmap;
            // Saving image to internal storage
            //DownloadImages.saveFile(context, bitmap, imageName);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            connection.disconnect();
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static void saveFile(Context context, Bitmap b, String picName){
        FileOutputStream fos;
        try {
            fos = context.openFileOutput(picName, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        }
        catch (FileNotFoundException e) {
            Log.d("LOG", "file not found");
            e.printStackTrace();
        }
        catch (IOException e) {
            Log.d("LOG", "io exception");
            e.printStackTrace();
        }
    }

    public static Bitmap loadBitmap(Context context, String picName){
        Bitmap b = null;
        FileInputStream fis;
        try {
            fis = context.openFileInput(picName);
            b = BitmapFactory.decodeStream(fis);
            fis.close();

        }
        catch (FileNotFoundException e) {
            Log.d("LOG", "file not found");
            e.printStackTrace();
        }
        catch (IOException e) {
            Log.d("LOG", "io exception");
            e.printStackTrace();
        }
        return b;
    }

    /*@Override
    protected Bitmap doInBackground(String... params) {
        String urldisplay = params[0];

        if (urldisplay == null || urldisplay.equals("")){
            return null;
        }

        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (MalformedURLException e) {
            //Wrong image URL
            e.printStackTrace();
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }*/

}
