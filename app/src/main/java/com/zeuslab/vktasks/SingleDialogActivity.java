package com.zeuslab.vktasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.CharacterPickerDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.methods.VKApiMessages;
import com.vk.sdk.api.model.VKApiMessage;
import com.zeuslab.vktasks.adapters.MessageAdapter;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by yatarkan on 18.02.2016.
 */
public class SingleDialogActivity extends AppCompatActivity {

    private ArrayList<VKApiMessage> message_history;
    private EditText editText;
    private RelativeLayout single_dialog_container;
    private ListView messages_list_view;
    private MessageAdapter messageAdapter;

    private int dialog_user_id;

    private int messages_offset = 0;
    private static final int messages_count = 20;
    private boolean flag_loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_dialog);
        //setTheme(R.style.AppTheme);
        Toolbar task_toolbar = (Toolbar) findViewById(R.id.dialog_toolbar);
        setSupportActionBar(task_toolbar);

        dialog_user_id = getIntent().getIntExtra("sender_id", 0);

        message_history = new ArrayList<>();

        messages_list_view = (ListView) this.findViewById(R.id.dialog_listview);
        messageAdapter = new MessageAdapter(SingleDialogActivity.this, message_history);

        editText = (EditText) findViewById(R.id.message_edittext);

        //Back button in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        String dialog_name = getIntent().getStringExtra("dialog_name");
        //Log.d("test", message);
        task_toolbar.setTitle(dialog_name);
        this.setTitle(dialog_name);

        VKRequest singleDialogRequest = new VKRequest("messages.getHistory", VKParameters.from(VKApiConst.USER_ID, dialog_user_id, VKApiConst.COUNT, messages_count, VKApiConst.OFFSET, messages_offset));
        singleDialogRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {

                try {
                    message_history.clear();
                    JSONArray jsonArray = response.json.getJSONObject("response").getJSONArray("items");
                    int length = jsonArray.length();
                    for (int i = 0; i < length; i++) {
                        VKApiMessage message = new VKApiMessage();
                        message.parse(jsonArray.getJSONObject(i));
                        message_history.add(message);
                        //Log.e("Message " + i, message.body);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("dialogs", e.toString());
                } finally {
                    messages_list_view.setAdapter(messageAdapter);
                    //Increment messages offset for further loading
                    messages_offset += messages_count;

                    messages_list_view.setOnScrollListener(new AbsListView.OnScrollListener() {

                        public void onScrollStateChanged(AbsListView view, int scrollState) {

                        }

                        public void onScroll(AbsListView view, int firstVisibleItem,
                                             int visibleItemCount, int totalItemCount) {

                            //if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0){
                            if (firstVisibleItem == 0) {
                                if (flag_loading == false) {
                                    flag_loading = true;
                                    Log.e("LOAD MESSAGES", "LOAD");
                                    loadMoreMessages(messages_offset, message_history);
                                    messages_offset += messages_count;
                                }
                            }
                        }
                    });



                }

            }

            @Override
            public void onError(VKError error) {
                Log.e("SingleDialog", error.toString());
            }
        });

    }

    @Override
    protected void onResume(){
        super.onResume();

    }

    private void loadMoreMessages(final int messages_offset, ArrayList<VKApiMessage> history){
        message_history = history;

        VKRequest singleDialogRequest = new VKRequest("messages.getHistory", VKParameters.from(VKApiConst.USER_ID, dialog_user_id, VKApiConst.COUNT, messages_count, VKApiConst.OFFSET, messages_offset));
        singleDialogRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {

                try {
                    JSONArray jsonArray = response.json.getJSONObject("response").getJSONArray("items");
                    int length = jsonArray.length();
                    for (int i = 0; i < length; i++) {
                        VKApiMessage message = new VKApiMessage();
                        message.parse(jsonArray.getJSONObject(i));
                        message_history.add(message);
                        //Log.e("Message " + i, message.body);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("dialogs", e.toString());
                } finally {
                    messageAdapter.notifyDataSetChanged();
                    messages_list_view.setSelection(messages_count);
                    flag_loading = false;
                }


            }
        });
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        super.onBackPressed();
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {

            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom()) {
                hideKeyboard(this);
                v.clearFocus();
            }

        }
        return super.dispatchTouchEvent(ev);
    }


}
