package com.zeuslab.vktasks.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zeuslab.vktasks.R;
import com.zeuslab.vktasks.SingleDialogActivity;
import com.zeuslab.vktasks.models.Dialog;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yatarkan on 13.02.2016.
 */
public class DialogAdapter extends BaseAdapter {

    private ArrayList<Dialog> dialogs;
    private LayoutInflater layoutInflater;
    private Context context;
    //public ImageLoader imageLoader;

    public DialogAdapter(Context context, ArrayList listData) {
        this.dialogs = listData;
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        //imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return dialogs.size();
    }

    @Override
    public Object getItem(int position) {
        return dialogs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /*View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_dialog, null);
        }*/

        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_dialog, null);
            holder = new ViewHolder();
            holder.relativeLayout = (RelativeLayout) convertView.findViewById(R.id.dialog_relative_layout);
            holder.dialogTitleView = (TextView) convertView.findViewById(R.id.dialog_title);
            holder.dialogBodyView = (TextView) convertView.findViewById(R.id.dialog_body);
            holder.dialogDateView = (TextView) convertView.findViewById(R.id.dialog_time);
            holder.imageView = (CircleImageView) convertView.findViewById(R.id.sender_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Dialog d = (Dialog) getItem(position);

        if (d.isGroupChat()) {
            holder.dialogTitleView.setText(d.getTitle());
        } else {
            holder.dialogTitleView.setText(d.getUserFullName());
        }
        holder.dialogBodyView.setText(d.getBody());
        holder.dialogDateView.setText(d.getDialogStringDate());


        if (d.getDialogPhoto().isEmpty()){
            holder.imageView.setImageResource(R.drawable.group_chat_default);
            holder.imageView.setBorderWidth(3);
            holder.imageView.setBorderColorResource(R.color.dialog_photo_border);
        } else {
            //Initializing ImageLoader
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(d.getDialogPhoto(), holder.imageView);
            holder.imageView.setBorderWidth(0);
        }

        //Change item background color if there are unread messages in the dialog
        if (d.hasUnreadMessages()) {
            holder.relativeLayout.setBackgroundResource(R.color.unread_messages);
        } else {
            holder.relativeLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context.getApplicationContext(), SingleDialogActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("dialog_name", d.isGroupChat() ? d.getTitle() : d.getUserFullName());
                intent.putExtra("dialog_photo", d.getDialogPhoto());
                intent.putExtra("sender_id", d.isGroupChat() ? d.getChatId()+2000000000 : d.getUserId());
                //intent.putExtra("chat_id", d.getChatId());
                context.getApplicationContext().startActivity(intent);
            }
        });

        return convertView;
        /*
        if (d != null) {
            //Set dialog info (title, message body, photo, etc)
            if (d.isGroupChat()){
                ((TextView) v.findViewById(R.id.dialog_title)).setText(d.getTitle());
            } else {
                ((TextView) v.findViewById(R.id.dialog_title)).setText(d.getUserFullName());
                //Set dialog photo
            }

            if (d.getDialogPhoto() != null) {
                new DownloadImages((CircleImageView) v.findViewById(R.id.sender_image))
                        .execute(d.getDialogPhoto());
            }

            ((TextView) v.findViewById(R.id.dialog_body)).setText(d.getBody() + "");
            ((TextView) v.findViewById(R.id.dialog_time)).setText(d.getDialogStringDate());
            //((ImageView) view.findViewById(R.id.ivImage)).setImageResource(p.image);
        }

        return v;
        */
    }

    static class ViewHolder {
        RelativeLayout relativeLayout;
        TextView dialogTitleView;
        TextView dialogBodyView;
        TextView dialogDateView;
        CircleImageView imageView;
    }
}
