package com.zeuslab.vktasks.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zeuslab.vktasks.R;
import com.zeuslab.vktasks.SingleTaskActivity;
import com.zeuslab.vktasks.models.Task;

import java.util.ArrayList;

/**
 * Created by 123 on 26.02.2016.
 */
public class TaskAdapter extends BaseAdapter {

    private ArrayList<Task> tasks;
    private LayoutInflater layoutInflater;
    private Context context;
    //public ImageLoader imageLoader;

    public TaskAdapter(Context context, ArrayList listData) {
        this.tasks = listData;
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public Object getItem(int position) {
        return tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return tasks.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(android.R.layout.simple_list_item_multiple_choice, null);
            holder = new ViewHolder();
            holder.taskTitleView = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Task t = (Task) getItem(position);

        holder.taskTitleView.setText( t.getName() );

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context.getApplicationContext(), SingleTaskActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(SingleTaskActivity.TASK_EXTRA, t.toJsonString());
                context.getApplicationContext().startActivity(intent);
            }
        });

        return convertView;
    }

    static class ViewHolder {

        TextView taskTitleView;

    }
}
