package com.zeuslab.vktasks.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vk.sdk.api.model.VKApiMessage;
import com.zeuslab.vktasks.R;
import com.zeuslab.vktasks.models.Dialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by 123 on 20.02.2016.
 */
public class MessageAdapter extends BaseAdapter {

    private ArrayList<VKApiMessage> messages;
    private LayoutInflater layoutInflater;
    private Context context;

    public MessageAdapter(Context context, ArrayList listData) {
        this.messages = listData;
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        //return messages.get(position);
        return messages.get(getCount() - position - 1);
    }

    @Override
    public long getItemId(int position) {

        return getCount() - position - 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_message, null);
            holder = new ViewHolder();
            holder.message_to_right = (View) convertView.findViewById(R.id.message_to_right);
            holder.message_main = (LinearLayout) convertView.findViewById(R.id.message_main);
            holder.message_photo = (ImageView) convertView.findViewById(R.id.message_photo);

            holder.message_sender = (TextView) convertView.findViewById(R.id.message_sender);
            holder.message_text = (TextView) convertView.findViewById(R.id.message_text);
            holder.message_time = (TextView) convertView.findViewById(R.id.message_time);
            holder.message_status = (ImageView) convertView.findViewById(R.id.message_status);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final VKApiMessage m = (VKApiMessage) getItem(position);

        holder.message_text.setText(m.body);

        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date date = new Date(m.date*1000L);
        holder.message_time.setText(format.format(date).toString());

        //Change position of message if it is NOT from current user
        if (m.out == true) {
            //Отправленное сообщение
            holder.message_main.setBackgroundResource(R.color.message_bg);
            holder.message_to_right.setVisibility(View.VISIBLE);
            holder.message_status.setVisibility(View.VISIBLE);
        } else {
            //Входящее сообщение
            holder.message_main.setBackgroundResource(R.drawable.message_border);
            holder.message_to_right.setVisibility(View.GONE);
            holder.message_status.setVisibility(View.GONE);
        }

        if (m.read_state == false) {
            //Message not read
            holder.message_status.setImageResource(R.drawable.tick_single);
        } else {
            holder.message_status.setImageResource(R.drawable.tick_double);
        }


        return convertView;
    }

    static class ViewHolder {
        View message_to_right;
        LinearLayout message_main;
        ImageView message_photo;
        TextView message_sender;
        TextView message_text;
        TextView message_time;
        ImageView message_status;
    }
}
