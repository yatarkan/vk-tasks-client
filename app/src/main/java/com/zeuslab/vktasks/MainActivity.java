package com.zeuslab.vktasks;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;


import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Badgeable;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKList;

import org.json.JSONException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    //public static String EXTRA_MESSAGE = "com.zeuslab.vktask.MESSAGE";

    private static final String[] vk_app_scope = new String[]{
            VKScope.MESSAGES,
            VKScope.STATUS
    };

    private Toolbar toolbar;

    private Drawer drawerResult = null;
    private PrimaryDrawerItem item_tasks = new PrimaryDrawerItem().withName(R.string.drawer_item_tasks);
    private PrimaryDrawerItem item_messagess = new PrimaryDrawerItem().withName(R.string.drawer_item_messages);
    private PrimaryDrawerItem item_newsfeed = new PrimaryDrawerItem().withName(R.string.drawer_item_newsfeed);
    /*private AccountHeader headerResult = new AccountHeaderBuilder()
            .withActivity(this)
            .withAccountHeader(R.layout.drawer_header)
            .build();*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initializing Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Authorizing in vk on first start
        if (!VKSdk.isLoggedIn()) {
            //Intent intent = new Intent(this, LoginActivity.class);
            VKSdk.login(this, vk_app_scope);
        }

        //Initializing Navigation Drawer
        drawerResult = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withTranslucentStatusBar(true)
                //.withTranslucentNavigationBar(true)
                //.withDisplayBelowStatusBar(true)
                .withActionBarDrawerToggleAnimated(true)
                //.withActionBarDrawerToggle(false)
                //.withAccountHeader(headerResult)
                .withHeader(R.layout.drawer_header)
                .addDrawerItems(
                        item_tasks
                                .withIcon(FontAwesome.Icon.faw_list_ul)
                                .withBadge("3")
                                .withIdentifier(1),
                        item_messagess
                                .withIcon(FontAwesome.Icon.faw_envelope)
                                .withBadge("")
                                .withIdentifier(2),
                        item_newsfeed
                                .withIcon(FontAwesome.Icon.faw_comments)
                                .withIdentifier(3),
                        new SectionDrawerItem()
                                .withName(R.string.drawer_item_more),
                        new SecondaryDrawerItem()
                                .withName(R.string.drawer_item_settings)
                                .withIcon(FontAwesome.Icon.faw_cog)
                                .withIdentifier(4),
                        //new SecondaryDrawerItem().withName(R.string.drawer_item_help).withIcon(FontAwesome.Icon.faw_question),
                        new SecondaryDrawerItem()
                                .withName(R.string.drawer_item_logout)
                                .withIcon(FontAwesome.Icon.faw_sign_out)
                                .withIdentifier(5),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem()
                                .withName(R.string.drawer_item_contact)
                                .withIcon(FontAwesome.Icon.faw_phone)
                                .withBadge("12+")
                                .withIdentifier(6)
                )
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Скрываем клавиатуру при открытии Navigation Drawer
                        InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    // Обработка нажатия
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            String title = MainActivity.this.getResources().getString(((Nameable) drawerItem).getName().getTextRes());
                            Toast.makeText(MainActivity.this, title, Toast.LENGTH_SHORT).show();
                            //Set screen title in toolbar
                            toolbar.setTitle(title);

                            //Tasks button pressed
                            if (drawerItem.getIdentifier() ==  1){
                                MainActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new FragmentTasks()).commit();
                            } else if (drawerItem.getIdentifier() ==  2){
                                //Messages button pressed
                                MainActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new FragmentMessages()).commit();
                            } else if (drawerItem.getIdentifier() == 5){
                                //Log out button pressed
                                VKSdk.logout();
                                Application.appSharedPreferences.edit().clear().commit();
                                //Reload current activity
                                finish();
                                startActivity(getIntent());
                            }
                        }
                        return false;
                        /*
                        if (drawerItem instanceof Badgeable) {
                            Badgeable badgeable = (Badgeable) drawerItem;
                            if (badgeable.getBadge() != null) {
                                // учтите, не делайте так, если ваш бейдж содержит символ "+"
                                try {
                                    int badge = Integer.valueOf(badgeable.getBadge());
                                    if (badge > 0) {
                                        drawerResult.updateBadge(String.valueOf(badge - 1), position);
                                    }
                                } catch (Exception e) {
                                    Log.d("test", "Не нажимайте на бейдж, содержащий плюс! :)");
                                }
                            }
                        }*/
                    }
                })
                .withOnDrawerItemLongClickListener(new Drawer.OnDrawerItemLongClickListener() {
                    @Override
                    // Обработка длинного клика, например, только для SecondaryDrawerItem
                    public boolean onItemLongClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof SecondaryDrawerItem) {
                            Toast.makeText(MainActivity.this, ((SecondaryDrawerItem) drawerItem).getName().getText().toString(), Toast.LENGTH_SHORT).show();
                        }
                        return false;
                    }
                })
                .build();

        //Fill side navigation with user information
        Application.setUserInfoInSideMenu(drawerResult.getHeader());
    }

    public void setMessagesItemBadge(int unread_messages){
        drawerResult.updateBadge(2, new StringHolder(unread_messages + ""));
    }

    //Заглушка
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Заглушка
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        if(drawerResult.isDrawerOpen()){
            drawerResult.closeDrawer();
        }
        else{
            super.onBackPressed();
        }
    }

    /*
    Обработка событий при авторизации пользователя в ВК
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(final VKAccessToken res) {
                // Пользователь успешно авторизовался
                Toast.makeText(MainActivity.this, "Авторизация прошла успешно", Toast.LENGTH_SHORT).show();

                VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.USER_ID, res.userId, VKApiConst.FIELDS,"photo_50"));
                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        //Do complete stuff
                        VKApiUser current_user = ((VKList<VKApiUser>)response.parsedModel).get(0);

                        //Save user date to shared preferences
                        SharedPreferences.Editor editor = Application.appSharedPreferences.edit();
                        editor.putString(Application.APP_PREFERENCES_USER_ID, res.userId);
                        editor.putString(Application.APP_PREFERENCES_USER_EMAIL, res.email);
                        editor.putString(Application.APP_PREFERENCES_USER_FIRST_NAME, current_user.first_name);
                        editor.putString(Application.APP_PREFERENCES_USER_LAST_NAME, current_user.last_name);
                        editor.putString(Application.APP_PREFERENCES_USER_PHOTO_50, current_user.photo_50);
                        editor.apply();

                        //Toast.makeText(MainActivity.this, current_user.photo_50, Toast.LENGTH_SHORT).show();

                        Application.setUserInfoInSideMenu(drawerResult.getHeader());
                    }
                    @Override
                    public void onError(VKError error) {
                        //Do error stuff

                    }
                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        //I don't really believe in progress
                    }
                });
            }
            @Override
            public void onError(VKError error) {
                // Произошла ошибка авторизации (например, пользователь запретил авторизацию)
                Toast.makeText(MainActivity.this, "Ошибка авторизации. " + error.errorMessage, Toast.LENGTH_LONG).show();
                //TODO star new activity with repeat button
                //Reload current activity
                finish();
                startActivity(getIntent());
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
