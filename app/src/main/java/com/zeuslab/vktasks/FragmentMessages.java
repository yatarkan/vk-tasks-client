package com.zeuslab.vktasks;

import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiGetDialogResponse;
import com.vk.sdk.api.model.VKApiGetMessagesResponse;
import com.vk.sdk.api.model.VKApiMessage;
import com.vk.sdk.api.model.VKList;
import com.zeuslab.vktasks.models.Dialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by yatarkan on 01.02.2016.
 */
public class FragmentMessages extends Fragment {

    ListView dialogs_list_view;
    //ArrayList <Dialog> dialogs_list = new ArrayList<Dialog>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView =  inflater.inflate(R.layout.fragment_messages, container, false);

        dialogs_list_view = (ListView) rootView.findViewById(R.id.dialogs_list);

        //Initializing ImageLoader
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getContext()));

        //fill messages
        LoadDialogsTask mLoadDialogsTask = new LoadDialogsTask(FragmentMessages.this, dialogs_list_view);
        mLoadDialogsTask.execute();


        return rootView;
    }
}
