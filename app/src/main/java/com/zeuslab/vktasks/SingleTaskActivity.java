package com.zeuslab.vktasks;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zeuslab.vktasks.models.Task;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

public class SingleTaskActivity extends AppCompatActivity {

    public static final String TASK_EXTRA = "com.zeuslab.vktask.TASK_EXTRA";
    private Task current_task;

    private TextView task_name_view;
    private TextView task_description_view;
    private Button task_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_task);
        setTheme(R.style.AppTheme);
        Toolbar task_toolbar = (Toolbar) findViewById(R.id.task_toolbar);
        setSupportActionBar(task_toolbar);

        task_name_view = (TextView) findViewById(R.id.task_name_view);
        task_description_view = (TextView) findViewById(R.id.task_description_view);
        task_send = (Button) findViewById(R.id.task_send);

        //Back button in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String task_json_string = intent.getStringExtra(TASK_EXTRA);
        current_task = new Task(task_json_string);
        //Log.d("test", message);
        task_toolbar.setTitle(current_task.getName());
        this.setTitle(current_task.getName());

        task_name_view.setText(current_task.getName());
        task_description_view.setText(current_task.getDescription());

        task_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new SendTask().execute("");
                sendPostRequest(getResources().getString(R.string.post_example));
                Log.d("JSON current task", current_task.toJsonString());


            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        super.onBackPressed();
        return true;
    }

    private class SendTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String[] params) {
            String requestURL = "http://vktasks-zeuslab.rhcloud.com/api/task";
            String response = "";


            HttpURLConnection httpcon = null;
            try {
                httpcon = (HttpURLConnection) new URL(requestURL).openConnection();
                httpcon.setDoOutput(true);
                httpcon.setDoInput(true);
                httpcon.setUseCaches(false);
                httpcon.setRequestProperty("Content-Type", "application/json");
                httpcon.setRequestProperty("Accept", "application/json");
                httpcon.setRequestMethod("POST");
                httpcon.connect();

                OutputStream os = httpcon.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                osw.write(current_task.toJsonString().toString());
                osw.flush();
                osw.close();


                //DataOutputStream outputStream = new DataOutputStream(httpcon.getOutputStream());


                Log.d("TASK JSON", current_task.toJsonString());
                byte[] outputBytes = current_task.toJsonString().getBytes("UTF-8");

                //outputStream.write(outputBytes);
                //outputStream.flush();
                //outputStream.close();


                //OutputStream os = httpcon.getOutputStream();
                //os.write(outputBytes);

                InputStream responseStream = new BufferedInputStream(httpcon.getErrorStream());
                BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
                String line = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((line = responseStreamReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                responseStreamReader.close();

                String r = stringBuilder.toString();
                Log.d("RESPONSE", r);

                //os.close();
                Log.d("RESPONSE CODE", httpcon.getResponseCode()+"");


            } catch (IOException e) {
                e.printStackTrace();
                Log.d("Error", e.toString());
            }

            //Log.d("SENDING TO SERVER", "Response: " + response);
            // do above Server call here
            return "some message";
        }

        @Override
        protected void onPostExecute(String message) {
            //process message
        }
    }

    private void sendPostRequest(String requestBody){
        String requestURL = "http://vktasks-zeuslab.rhcloud.com/api/task";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, requestURL, requestBody, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // TODO Auto-generated method stub
                //txtDisplay.setText("Response => "+response.toString());
                //findViewById(R.id.progressBar1).setVisibility(View.GONE);
                Log.d("POST Response", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                Log.d("POST Error", error.toString());
            }
        });

        queue.add(jsObjRequest);
    }

    private void sendGetRequest(){
        String requestURL = "http://vktasks-zeuslab.rhcloud.com/api/task/all/966";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, requestURL, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // TODO Auto-generated method stub
                //txtDisplay.setText("Response => "+response.toString());
                //findViewById(R.id.progressBar1).setVisibility(View.GONE);
                Log.d("GET Response", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                Log.d("GET Error", error.getMessage());
            }
        });

        queue.add(jsObjRequest);
    }

}
