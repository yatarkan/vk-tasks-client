package com.zeuslab.vktasks;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * Created by yatarkan on 02.03.2016.
 */
public class DialogPriority extends DialogFragment implements View.OnClickListener {

    final String LOG_TAG = "DialogPriority";

    private Button imp_urg_view;
    private Button imp_not_urg_view;
    private Button not_imp_urg_view;
    private Button not_imp_not_urg_view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Выберите приоритет");
        View v = inflater.inflate(R.layout.dialog_priority, null);
        imp_urg_view = (Button) v.findViewById(R.id.imp_urg_view);
        imp_not_urg_view = (Button) v.findViewById(R.id.imp_not_urg_view);
        not_imp_urg_view = (Button) v.findViewById(R.id.not_imp_urg_view);
        not_imp_not_urg_view = (Button) v.findViewById(R.id.not_imp_not_urg_view);

        imp_urg_view.setOnClickListener(this);
        imp_not_urg_view.setOnClickListener(this);
        not_imp_urg_view.setOnClickListener(this);
        not_imp_not_urg_view.setOnClickListener(this);

        return v;
    }

    public void onClick(View v) {
        String mes = (String) ((Button) v).getText();
        Log.d(LOG_TAG, "Dialog: " + mes);
        mListener.onDialogPriorityClick(DialogPriority.this, mes);
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        //Log.d(LOG_TAG, "Dialog: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        //Log.d(LOG_TAG, "Dialog: onCancel");
    }

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface DialogPriorityListener {
        public void onDialogPriorityClick(DialogFragment dialog, String message);
    }

    // Use this instance of the interface to deliver action events
    DialogPriorityListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogPriorityListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement DialogPriorityListener");
        }
    }
}
