package com.zeuslab.vktasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yatarkan on 10.02.2016.
 */
public class Application extends android.app.Application {

    public static final String APP_PREFERENCES = "app_settings";
    public static final String APP_PREFERENCES_USER_ID = "user_id";
    public static final String APP_PREFERENCES_USER_EMAIL = "user_email";
    public static final String APP_PREFERENCES_USER_FIRST_NAME = "user_first_name";
    public static final String APP_PREFERENCES_USER_LAST_NAME = "user_last_name";
    public static final String APP_PREFERENCES_USER_PHOTO_50 = "user_photo_50";

    public static SharedPreferences appSharedPreferences;
    /*
    public static String user_id;
    public static String user_email;
    public static String user_first_name;
    public static String user_last_name;
    public static String user_photo50;
    */

    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
                // VKAccessToken is invalid
                Toast.makeText(Application.this, "AccessToken invalidated", Toast.LENGTH_LONG).show();
                //Intent intent = new Intent(Application.this, VkMessager.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //startActivity(intent);

            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        vkAccessTokenTracker.startTracking();
        //Initializing VK SDK
        VKSdk.initialize(this);

        appSharedPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static void setUserInfoInSideMenu(View v){
        //Fill side navigation with user information
        //Set user name in side menu
        TextView user_name_label = (TextView) v.findViewById(R.id.user_name);
        if (appSharedPreferences.contains(APP_PREFERENCES_USER_FIRST_NAME) &&
                appSharedPreferences.contains(APP_PREFERENCES_USER_LAST_NAME) ) {
            user_name_label.setText(appSharedPreferences.getString(APP_PREFERENCES_USER_FIRST_NAME,"User")
                    + " "
                    + appSharedPreferences.getString(APP_PREFERENCES_USER_LAST_NAME,"User"));
        }

        //Set user photo in side menu
        if (appSharedPreferences.contains(APP_PREFERENCES_USER_PHOTO_50)) {
            new DownloadImages((CircleImageView) v.findViewById(R.id.profile_image))
                    .execute(appSharedPreferences.getString(APP_PREFERENCES_USER_PHOTO_50, ""));
        }
    }

}
