package com.zeuslab.vktasks.models;

import com.vk.sdk.api.model.VKApiDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by yatarkan on 14.02.2016.
 */
public class Dialog {
    private int user_id;
    private int chat_id;
    private String user_full_name;
    private String dialog_photo = null;
    private String title;
    private String body;
    private Date dialog_date;
    private Boolean is_group_chat;
    private Boolean unread_messages;

    public Dialog() {}

    public Dialog(int user_id, String title, String body) {
        this.user_id = user_id;
        this.title = title;
        this.body = body;
        this.is_group_chat = false;
    }

    public Dialog(VKApiDialog dialog) {
        this.user_id = dialog.message.user_id;
        this.title = dialog.message.title;
        this.body = dialog.message.body;
        this.dialog_date = new Date((long)dialog.message.date*1000);
        if (dialog.message.title.equals(" ... ") ){ //is dialog
            this.is_group_chat = false;
        } else { //is group chat
            this.is_group_chat = true;
            //this.dialog_photo = dialog.message.fields.get("photo_100");
        }
    }

    public Dialog(JSONObject item){
        try {
            String chatId = item.getJSONObject("message").optString("chat_id");
            String userId = item.getJSONObject("message").optString("user_id");
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public int getUserId() {
        return user_id;
    }

    public void setUserId(int userId) {
        this.user_id = userId;
    }

    public void setUserId(String userId) {
        this.user_id = Integer.parseInt(userId);
    }

    public String getUserFullName() {
        return user_full_name;
    }

    public void setUserFullName(String user_full_name) {
        this.user_full_name = user_full_name;
    }

    public String getDialogPhoto() {
        return dialog_photo;
    }

    public void setDialogPhoto(String dialog_photo_url) {
        this.dialog_photo = dialog_photo_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Boolean isGroupChat(){
        return is_group_chat;
    }

    public void setGroupChat(Boolean is_group){
        this.is_group_chat = is_group;
    }

    public Date getDialogDate() {
        return dialog_date;
    }

    public void setDialogDate(Date dialog_date) {
        this.dialog_date = dialog_date;
    }

    public void setDialogDate(String date_str) {
        this.dialog_date = new Date( (long) Long.parseLong(date_str)*1000);
    }

    public String getDialogStringDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Moscow"));
        Date date_now = new Date();
        long diff = date_now.getTime() - this.dialog_date.getTime();

        if ((diff / (24 * 60 * 60 * 1000)) >= 7 ){
            return dateFormat.format(this.dialog_date) + "";
        } else if ( (diff / (24 * 60 * 60 * 1000)) >= 1 ) {
            dateFormat = new SimpleDateFormat("E");
            return dateFormat.format(this.dialog_date) + "";
        } else {
            if (dateFormat.format(this.dialog_date).compareTo(dateFormat.format(date_now)) != 0) {
                dateFormat = new SimpleDateFormat("E");
                return dateFormat.format(this.dialog_date) + "";
            }
            dateFormat = new SimpleDateFormat("HH:mm");
            return dateFormat.format(this.dialog_date) + "";
        }

    }

    public int getChatId() {
        return chat_id;
    }

    public void setChatId(int chat_id) {
        this.chat_id = chat_id;
    }

    public void setChatId(String chat_id) {
        this.chat_id = Integer.parseInt(chat_id);
    }

    public Boolean hasUnreadMessages() {
        return unread_messages;
    }

    public void setUnreadMessages(Boolean unread_messages) {
        this.unread_messages = unread_messages;
    }
}
