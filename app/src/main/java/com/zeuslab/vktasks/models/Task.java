package com.zeuslab.vktasks.models;

import android.content.SharedPreferences;
import android.util.Log;

import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.zeuslab.vktasks.Application;
import com.zeuslab.vktasks.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by yatarkan on 24.02.2016.
 */
public class Task {

    private int id;
    private String user_id;
    private String name;
    private String description;
    private Priorities priority; // Срочные, важные, не срочные, не важные
    private Date created;
    private Date deadline;
    private Date started;
    private Date done;
    private ArrayList<String> tags;
    private int status; // to do, in progress, done DEPRECATED?
    private boolean current;

    //Minimum constructor
    public Task(String name, String description){

        this.id = 0;

        if (Application.appSharedPreferences.contains(Application.APP_PREFERENCES_USER_ID)){
            this.user_id = Application.appSharedPreferences.getString(Application.APP_PREFERENCES_USER_ID, "");
        }
        this.name = name;
        this.description = description;

        this.priority = Priorities.NOT_IMPORTANT_NOT_URGENT;
        this.created = new Date(System.currentTimeMillis());
        this.deadline = null;
        this.started = null;
        this.done = null;

        this.tags = new ArrayList<String>();

        this.status = 0;
        this.current = false;
    }

    public Task(String title, Priorities priority, Calendar deadline){
        this.id = 0;

        if (Application.appSharedPreferences.contains(Application.APP_PREFERENCES_USER_ID)){
            this.user_id = Application.appSharedPreferences.getString(Application.APP_PREFERENCES_USER_ID, "");
        }
        this.name = title;
        this.description = "";

        this.priority = priority;
        this.created = new Date(System.currentTimeMillis());
        this.deadline = deadline.getTime();
        this.started = null;
        this.done = null;

        this.tags = new ArrayList<String>();

        this.status = 0;
        this.current = false;
    }

    public Task(String title, String description, Priorities priority, Calendar deadline){
        this.id = 0;

        if (Application.appSharedPreferences.contains(Application.APP_PREFERENCES_USER_ID)){
            this.user_id = Application.appSharedPreferences.getString(Application.APP_PREFERENCES_USER_ID, "");
        }
        this.name = title;
        this.description = description;

        this.priority = priority;
        this.created = new Date(System.currentTimeMillis());
        this.deadline = deadline.getTime();
        this.started = null;
        this.done = null;

        this.tags = new ArrayList<String>();

        this.status = 0;
        this.current = false;
    }

    public Task(String json_string){
        try {
            JSONObject jsonObject = new JSONObject(json_string);
            this.id = jsonObject.getInt("task_id");
            this.user_id = jsonObject.getString("user_id");
            this.name = jsonObject.getString("title");;
            this.description = jsonObject.getString("description");;

            this.priority = Priorities.getEnum(jsonObject.getString("priority"));
            JSONArray time = jsonObject.getJSONArray("time");
            this.created = new Date(time.getJSONObject(0).optLong("created"));
            this.deadline = new Date(time.getJSONObject(1).optLong("deadline"));
            this.started = new Date(time.getJSONObject(2).optLong("started"));
            this.done = new Date(time.getJSONObject(3).optLong("done"));

            JSONArray tags = jsonObject.optJSONArray("tags");
            this.tags = new ArrayList<String>();
            if (tags != null) {
                for (int i = 0; i < tags.length(); i++) {
                    this.tags.add(tags.getString(i));
                }
            }

            this.status = jsonObject.getInt("status");
            this.current = jsonObject.getBoolean("person_in_progress");

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("TAG", "JSON task parsing error: " + e.getLocalizedMessage());
        }
    }

    public JSONObject dumpJson() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("task_id", this.id+"");
        result.put("user_id", this.user_id);
        result.put("title", this.name);
        result.put("description", this.description);
        result.put("priority", this.priority.toString());

        JSONArray time = new JSONArray();
        time.put(this.getCreatedJson());
        time.put(this.getDeadlineJson());
        time.put(this.getStartedJson());
        time.put(this.getDoneJson());
        result.put("time", time);

        result.put("tags", this.getTagsJson());

        result.put("status", this.status+"");
        result.put("person_in_progress", this.current+"");

        return result;
    }

    public static JSONObject saveTasks(ArrayList<Task> tasks) throws JSONException {
        JSONObject result = new JSONObject();
        JSONArray a = new JSONArray();
        for(Task t : tasks){
            a.put(t.dumpJson());
        }
        result.accumulate("tasks", a);
        return result;
    }

    public Date getCreated() {
        return created;
    }

    public JSONObject getCreatedJson() throws JSONException{
        JSONObject t = new JSONObject();
        t.put("created", new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ").format(this.created));
        return t;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public JSONObject getDeadlineJson() throws JSONException {
        JSONObject t = new JSONObject();
        if (this.deadline != null){
            t.put("deadline", new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ").format(this.deadline));
        } else {
            t.put("deadline", "");
        }
        return t;
    }

    public JSONObject getStartedJson() throws JSONException {
        JSONObject t = new JSONObject();
        if (this.started != null){
            t.put("started", new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ").format(this.started));
        } else {
            t.put("started", "");
        }
        return t;
    }

    public JSONObject getDoneJson() throws JSONException {
        JSONObject t = new JSONObject();
        //t.put("done", this.done.getTime());
        if (this.done != null){
            t.put("done", new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ").format(this.done));
        } else {
            t.put("done", "");
        }
        return t;
    }

    public JSONArray getTagsJson() throws JSONException{
        if (this.tags == null) return null;
        JSONArray tags = new JSONArray();
        for (String s : this.tags) {
            tags.put(s);
        }
        return tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String new_name){
        this.name = new_name;
    }

    public String getDescription(){
        return this.description;
    }

    public void setDescription(String new_description){
        this.description = new_description;
    }

    public String toJsonString(){
        try {
            return this.dumpJson().toString();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("TAG", "Error converting task object to json string");
        }
        return null;
    }

    public enum Priorities{
        IMPORTANT_URGENT("imp_urg"),
        IMPORTANT_NOT_URGENT("imp_n_urg"),
        NOT_IMPORTANT_URGENT("n_imp_urg"),
        NOT_IMPORTANT_NOT_URGENT("n_imp_n_urg")
        ;

        private final String text;

        /**
         * @param text
         */
        private Priorities(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }

        public static Priorities getEnum(String s) {

            switch (s) {
                case "imp_urg":
                    return IMPORTANT_URGENT;
                case "imp_n_urg":
                    return IMPORTANT_NOT_URGENT;
                case "n_imp_urg":
                    return NOT_IMPORTANT_URGENT;
                case "n_imp_n_urg":
                    return NOT_IMPORTANT_NOT_URGENT;
                default:
                    return null;
            }
        }

        public static Priorities getEnum(int res_id) {

            switch (res_id) {
                case R.string.priority_imp_urg:
                    return IMPORTANT_URGENT;
                case R.string.priority_imp_not_urg:
                    return IMPORTANT_NOT_URGENT;
                case R.string.priority_not_imp_urg:
                    return NOT_IMPORTANT_URGENT;
                case R.string.priority_not_imp_not_urg:
                    return NOT_IMPORTANT_NOT_URGENT;
                default:
                    return null;
            }
        }
    }
}
