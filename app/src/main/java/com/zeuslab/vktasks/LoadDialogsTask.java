package com.zeuslab.vktasks;

import android.os.AsyncTask;
import android.widget.AbsListView;
import android.widget.ListView;

import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.zeuslab.vktasks.adapters.DialogAdapter;
import com.zeuslab.vktasks.models.Dialog;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by yatarkan on 16.02.2016.
 */
public class LoadDialogsTask extends AsyncTask< Integer, Void, ArrayList<Dialog>> {

    private final FragmentMessages context;
    private ListView dialogs_list_view;
    private ArrayList<Dialog> dialogs_list;
    private DialogAdapter dialogAdapter;


    private int messages_offset = 0;
    private static final int messages_count = 20;
    private boolean flag_loading = false;

    public LoadDialogsTask (final FragmentMessages context, ListView dialogs_list_view){
        super();
        this.context = context;
        this.dialogs_list = new ArrayList<>();
        this.dialogs_list_view = dialogs_list_view;
        dialogAdapter = new DialogAdapter(this.context.getContext(), dialogs_list);
        dialogs_list_view.setAdapter(dialogAdapter);

        this.dialogs_list_view.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0)
                {
                    if(flag_loading == false)
                    {
                        flag_loading = true;
                        loadDialogs(messages_offset,messages_count, dialogs_list);
                    }
                }
            }
        });
    }

    @Override
    protected ArrayList<Dialog> doInBackground(Integer... params) {
        ArrayList<Dialog> data = loadDialogs(messages_offset, messages_count, dialogs_list);

        return data;
    }

    @Override
    protected void onPostExecute(ArrayList<Dialog> dialogs) {
        if (dialogs.isEmpty()) {
            //Toast.makeText(context.getContext(), "Error loading dialogs", Toast.LENGTH_LONG).show();
            return;
        }

        //ListView dialogs_list_view = (ListView) context.getActivity().findViewById(R.id.dialogs_list);
        //DialogAdapter dialogAdapter = new DialogAdapter(context.getContext(), dialogs);
        //dialogs_list_view.setAdapter(dialogAdapter);

    }

    private ArrayList<Dialog> loadDialogs(final int offset, int count, ArrayList<Dialog> dialogs){

        dialogs_list = dialogs;

        VKRequest getDialogsRequest = VKApi.messages().getDialogs(VKParameters.from(VKApiConst.OFFSET, offset, VKApiConst.COUNT, count));
        getDialogsRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                //Do complete stuff
                String dialog_owners_str = "";
                /*VKApiGetDialogResponse messagesResponse = (VKApiGetDialogResponse) response.parsedModel;


                for (VKApiDialog dialog : messagesResponse.items) {
                    Dialog temp = new Dialog(dialog);
                    dialogs_list.add(temp);
                    if (!temp.isGroupChat()) {
                        dialog_owners_str += dialog.message.user_id + ",";
                    }
                }*/
                try {
                    int unread_dialogs = response.json.getJSONObject("response").optInt("unread_dialogs");
                    MainActivity act = (MainActivity) context.getActivity();
                    //Set new messages count in side navigation menu
                    act.setMessagesItemBadge(unread_dialogs);

                    JSONArray jsonDialogsArray = response.json.getJSONObject("response").getJSONArray("items");

                    for (int i = 0; i < jsonDialogsArray.length(); i++) {
                        Dialog new_dialog = new Dialog();
                        String user_id = jsonDialogsArray.getJSONObject(i).getJSONObject("message").optString("user_id");
                        String chat_id = jsonDialogsArray.getJSONObject(i).getJSONObject("message").optString("chat_id");
                        String title = jsonDialogsArray.getJSONObject(i).getJSONObject("message").optString("title");
                        String body = jsonDialogsArray.getJSONObject(i).getJSONObject("message").optString("body");
                        String date = jsonDialogsArray.getJSONObject(i).getJSONObject("message").optString("date");

                        String out = jsonDialogsArray.getJSONObject(i).getJSONObject("message").optString("out");
                        String read_state = jsonDialogsArray.getJSONObject(i).getJSONObject("message").optString("read_state");

                        new_dialog.setBody(body);
                        new_dialog.setDialogDate(date);
                        //If dialog has unread messages
                        if (Integer.parseInt(out)==0 && Integer.parseInt(read_state)==0){
                            new_dialog.setUnreadMessages(true);
                        } else {
                            new_dialog.setUnreadMessages(false);
                        }

                        // If parsed dialog is NOT a group chat
                        if (chat_id.equals("")) {
                            dialog_owners_str += user_id + ",";
                            new_dialog.setUserId(user_id);
                            new_dialog.setGroupChat(false);
                        }
                        // If parsed dialog IS group chat
                        else {
                            dialog_owners_str += chat_id + ",";
                            new_dialog.setChatId(chat_id);
                            new_dialog.setGroupChat(true);
                            new_dialog.setTitle(title);
                            String image_url = jsonDialogsArray.getJSONObject(i).getJSONObject("message").optString("photo_100");
                            new_dialog.setDialogPhoto(image_url);
                        }
                        dialogs_list.add(new_dialog);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }

                    VKRequest getUsersRequest = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, dialog_owners_str, VKApiConst.FIELDS, "photo_50"));
                getUsersRequest.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        //Do complete stuff
                        try {
                            JSONArray jsonDialogSendersArray = response.json.getJSONArray("response");
                            for (int i = 0; i < jsonDialogSendersArray.length(); i++) {                                           // getting user ids
                                if (dialogs_list.get(i + messages_offset).isGroupChat())
                                    continue;
                                StringBuilder fullUserName = new StringBuilder();
                                String userName = jsonDialogSendersArray.getJSONObject(i).optString("first_name");
                                String userSurname = jsonDialogSendersArray.getJSONObject(i).optString("last_name");
                                fullUserName.append(userName).append(" ").append(userSurname);
                                dialogs_list.get(i + messages_offset).setUserFullName(fullUserName.toString());
                                dialogs_list.get(i + messages_offset).setDialogPhoto(jsonDialogSendersArray.getJSONObject(i).optString("photo_50"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //DialogAdapter dialogAdapter = new DialogAdapter(context.getContext(), R.layout.item_dialog, dialogs_list);
                        dialogAdapter.notifyDataSetChanged();

                        //Increment messages offset for further loading
                        messages_offset += messages_count;
                        flag_loading = false;


                    }

                    @Override
                    public void onError(VKError error) {
                        //Do error stuff

                    }

                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        //I don't really believe in progress
                    }

                });


            }

            @Override
            public void onError(VKError error) {
                //Do error stuff

            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                //I don't really believe in progress
            }
        });
        return dialogs_list;
    }


}
