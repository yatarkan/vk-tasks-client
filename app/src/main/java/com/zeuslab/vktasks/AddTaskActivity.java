package com.zeuslab.vktasks;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.icons.MaterialDrawerFont;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.zeuslab.vktasks.DialogPriority.DialogPriorityListener;
import com.zeuslab.vktasks.models.Task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AddTaskActivity extends AppCompatActivity implements DialogPriorityListener {

    private EditText task_name;
    private TextInputLayout task_description_wrapper;
    private TextInputLayout task_priority_wrapper;
    private DialogFragment dialog_priority;
    private Task.Priorities priority = null;
    private TextInputLayout task_deadline_wrapper;
    private Calendar deadline = null;
    private Button save_button;

    private String fileName = "tasks.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_task);
        setSupportActionBar(toolbar);

        task_name = (EditText) findViewById(R.id.task_name);
        task_description_wrapper = (TextInputLayout) findViewById(R.id.task_description_layout);
        task_deadline_wrapper = (TextInputLayout) findViewById(R.id.task_deadline_layout);
        task_priority_wrapper = (TextInputLayout) findViewById(R.id.task_priority_layout);
        //Set icons
        ((ImageView) findViewById(R.id.task_description_icon)).setImageDrawable(new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_border_color)
                .color(Color.GRAY)
                .sizeDp(24));
        ((ImageView) findViewById(R.id.task_priority_icon)).setImageDrawable(new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_error)
                .color(Color.GRAY)
                .sizeDp(24));
        ((ImageView) findViewById(R.id.task_deadline_icon)).setImageDrawable(new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_schedule)
                .color(Color.GRAY)
                .sizeDp(24));

        ((RelativeLayout) findViewById(R.id.new_task_layout)).requestFocus();

        task_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (task_name.getText().toString().isEmpty()) {
                        Log.d("TAG", "Empty task title");
                        Snackbar snackbar = Snackbar.make(v, "Введите название задачи!", Snackbar.LENGTH_SHORT);
                        ViewGroup vg = (ViewGroup) snackbar.getView();
                        vg.setBackgroundColor(Color.RED);
                        snackbar.show();
                    }
                    hideKeyboard(v);
                }
            }
        });

        task_description_wrapper.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        dialog_priority = new DialogPriority();
        task_priority_wrapper.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_priority.show(getFragmentManager(), "dialog_priority");
            }
        });

        task_deadline_wrapper.getEditText().setText("Завтра " + new SimpleDateFormat("(dd.MM.yy), HH:mm").format(getTommorrowDate()));
        task_deadline_wrapper.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        save_button = (Button) findViewById(R.id.task_save);
        //save_button.setCompoundDrawables(new IconicsDrawable(this).icon(GoogleMaterial.Icon.gmd_save).color(Color.RED).sizeDp(24), null, null, null);
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKeyboard(v);

                String title = task_name.getText().toString();
                String description = task_description_wrapper.getEditText().getText().toString();
                //Set priority from EditText view
                //String s = task_priority_wrapper.getEditText().getText().toString();



                //TODO Check and handle empty input
                if (title.isEmpty()){
                    Log.d("TAG", "Empty task title");
                    Snackbar snackbar = Snackbar.make(v, "Введите название задачи!", Snackbar.LENGTH_SHORT);
                    ViewGroup vg = (ViewGroup) snackbar.getView();
                    vg.setBackgroundColor(Color.RED);
                    snackbar.show();
                    return;
                }
                if (task_priority_wrapper.getEditText().getText().toString().isEmpty() || priority == null){
                    Log.d("TAG", "Empty task priority");
                    task_priority_wrapper.setError("Приоритет не выбран!");
                    return;
                }

                Task task = new Task(title, description, priority, deadline);

                //Clear error messages under EditText views
                //task_name_wrapper.setError(null);
                task_deadline_wrapper.setError(null);

                Log.d("NEW TASK LOG", task.toJsonString());

                //TODO Delete temp json file
                /*Task task = new Task(task_name.getText().toString(), task_description.getText().toString() );
                ArrayList<Task> tasks = new ArrayList<Task>();

                try {
                    if (getData(getApplicationContext()) != null) {
                        JSONObject tasks_json = new JSONObject(getData(getApplicationContext()));
                        JSONArray tasks_array = tasks_json.optJSONArray("tasks");
                        for (int i = 0; i < tasks_array.length(); i++){
                            tasks.add( new Task(tasks_array.getJSONObject(i).toString()) );
                        }
                    }
                    tasks.add(task);
                    String res = Task.saveTasks(tasks).toString();
                    Log.e("JSON", res);
                    saveData(getApplicationContext(), res);

                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

                AddTaskActivity.this.finish();
                onSupportNavigateUp();

            }
        });
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        super.onBackPressed();
        return true;
    }

    private Date getTommorrowDate(){
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        deadline = calendar;
        return calendar.getTime();
    }

    //@Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String time = "You picked the following time: "+hourOfDay+"h"+minute;
        Log.d("TIME", time);
        //timeTextView.setText(time);
    }



    private void showDatePicker() {
        final Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        final Calendar cal = Calendar.getInstance();
                        cal.set(year, monthOfYear, dayOfMonth);
                        deadline = now;
                        //filter.setDate(cal.getTime());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showTimePicker(cal);
                            }
                        }, 300);
                        String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
                        Log.d("DATE", date);
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Выберите дату:");
    }

    private void showTimePicker(Calendar cal){
        final Calendar calendar = cal;
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                        Calendar calendar_time = calendar;
                        calendar_time.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar_time.set(Calendar.MINUTE,minute);
                        deadline = calendar_time;
                        task_deadline_wrapper.getEditText().setText(new SimpleDateFormat("EEEE (dd.MM.yy), HH:mm").format(calendar_time.getTime()));
                    }
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
        );
        tpd.show(getFragmentManager(), "Выберите время:");
    }

    public void saveData(Context context, String mJsonResponse) {
        try {
            FileWriter file = new FileWriter(context.getFilesDir().getPath() + "/" + fileName);
            file.write(mJsonResponse);
            file.flush();
            file.close();
        } catch (IOException e) {
            Log.e("TAG", "Error in Writing: " + e.getLocalizedMessage());
        }
    }

    public String getData(Context context) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + fileName);
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException e) {
            Log.e("TAG", "Error in Reading: " + e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public void onDialogPriorityClick(DialogFragment dialogFragment, String message) {
        task_priority_wrapper.getEditText().setText(message);

        if (message.equals(getResources().getString(R.string.priority_imp_urg))){
            priority = Task.Priorities.getEnum(R.string.priority_imp_urg);
            setPriorityIconColor(getResources().getColor(R.color.priority_imp_urg));
        } else if (message.equals(getResources().getString(R.string.priority_imp_not_urg))){
            priority = Task.Priorities.getEnum(R.string.priority_imp_not_urg);
            setPriorityIconColor(getResources().getColor(R.color.priority_imp_not_urg));
        } else if (message.equals(getResources().getString(R.string.priority_not_imp_urg))){
            priority = Task.Priorities.getEnum(R.string.priority_not_imp_urg);
            setPriorityIconColor(getResources().getColor(R.color.priority_not_imp_urg));
        } else if (message.equals(getResources().getString(R.string.priority_not_imp_not_urg))){
            priority = Task.Priorities.getEnum(R.string.priority_not_imp_not_urg);
            setPriorityIconColor(getResources().getColor(R.color.priority_not_imp_not_urg));
        }

        if (task_priority_wrapper.getEditText().getText().toString().isEmpty()){
            task_priority_wrapper.setErrorEnabled(true);
        } else {
            task_priority_wrapper.setErrorEnabled(false);
        }
    }

    private void setPriorityIconColor(int color_resourse){
        ((ImageView) findViewById(R.id.task_priority_icon)).setImageDrawable(new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_error)
                .color(color_resourse)
                .sizeDp(24));
    }

    private void hideKeyboard(View view) {
        //View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
