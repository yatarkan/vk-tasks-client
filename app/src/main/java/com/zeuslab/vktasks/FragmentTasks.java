package com.zeuslab.vktasks;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.LayoutInflaterCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ListView;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.context.IconicsLayoutInflater;
import com.zeuslab.vktasks.adapters.TaskAdapter;
import com.zeuslab.vktasks.models.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by yatarkan on 01.02.2016.
 */
public class FragmentTasks extends Fragment {

    private FloatingActionButton fab_add_task;
    private ArrayList<Task> tasks_list;
    private TaskAdapter taskAdapter;
    private String fileName = "tasks.json";

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_tasks, container, false);

        fab_add_task = (FloatingActionButton) rootView.findViewById(R.id.fab_add_task);
        
        fab_add_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(inflater.getContext(), AddTaskActivity.class);
                startActivity(intent);
            }
        });

        tasks_list = new ArrayList<Task>();
        taskAdapter = new TaskAdapter(getContext(), tasks_list);
        //JSONArray tasks_json_array = null;

        //String json_string = getString(R.string.json_test);
        /*
        try {
            if (getData(getContext()) != null) {
                JSONObject tasks_json = new JSONObject(getData(getContext()));
                JSONArray tasks_array = tasks_json.optJSONArray("tasks");
                for (int i = 0; i < tasks_array.length(); i++){
                    tasks_list.add( tasks_array.getJSONObject(i).getString("title") );
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        ListView task_list_view = (ListView) rootView.findViewById(R.id.task_list);

        task_list_view.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        task_list_view.setAdapter(taskAdapter);

        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG", "FragmentTask onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d("TAG", "FragmentTask onStart");
        try {
            if (getData(getContext()) != null) {
                tasks_list.clear();
                JSONObject tasks_json = new JSONObject(getData(getContext()));
                JSONArray tasks_array = tasks_json.optJSONArray("tasks");
                for (int i = 0; i < tasks_array.length(); i++){
                    tasks_list.add( new Task(tasks_array.getJSONObject(i).toString()) );
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        taskAdapter.notifyDataSetChanged();
    }

    public void onResume() {
        super.onResume();
        Log.d("TAG", "FragmentTask onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d("TAG", "FragmentTask onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d("TAG", "FragmentTask onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TAG", "FragmentTask onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d("TAG", "FragmentTask onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        Log.d("TAG", "FragmentTask onDetach");
    }


    public String getData(Context context) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + fileName);
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException e) {
            Log.e("TAG", "Error in Reading: " + e.getLocalizedMessage());
            return null;
        }
    }

}
